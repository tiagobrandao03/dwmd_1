<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    
    <link rel="stylesheet" href="css/count3_estilo.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Mendes e Castro Construtora</title>

    <style>
        /* font-family: 'Roboto', sans-serif;
    font-family: 'Oswald', sans-serif; */
        .swiper-container {
          width: 100%;
          height: 100%;
        }
    
        .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;
    
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
    
        .swiper-pagination-progressbar 
        .swiper-pagination-progressbar-fill {
          background: rgba(144,28,64,0.7);
        }
    
        .swiper-button-prev .swiper-button-next {
          color:#fff;
        }
    
        :root{--swiper-theme-color:rgb(144,28,64)}
    
      </style>
      
</head>
<body>
<header>
  <div class="container-fluid" >
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3 logo-margin" style="padding-left:7%;"><a href="index.php">
          <img src="img/7a1b9e8a-2f9b-48ab-859b-590c70c2b56b.jpg" alt="Logomarca" class="logo" title="Mendes e Castro Construtora"></a>
        </div>
        <div class="col-md-12 nav-margin mt-5">
          <nav class="d-flex justify-content-end align-items-end header-margin m-0">
            <!--   -->
            <ul class="mt-5">
              <div class="row">
              <li class="col-md-1 col-sm-12"><a href="index.php" class="home link-active">Home</a></li>
              <li class="col-md-2 col-sm-12"><a href="lancamentos.php" class="lancamentos">Lançamentos</a></li>
              <!-- <li><a href="#">Em Construção</a></li> -->
              <li class="col-md-2 col-sm-12"><a href="prontosparamorar.php" class="prontosparamorar">Prontos para Morar</a></li>
              <li class="col-md-2 col-sm-12"><a href="apartamentosavulsos.php" class="apartamentosavulsos">Apartamentos Avulsos</a></li>
              <li class="col-md-2 col-sm-12"><a href="quem_somos.php" class="quem_somos">Quem Somos</a></li>
              <li class="col-md-2 col-sm-12"><a href="contato.php" class="contato">Contato</a></li>
              </div>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>