<?php 
  include("includes/header.php");
?>
<!-- Começo vilaggio -->
<div class="container-fluid">

<div class="row">
  <div class="carousel-item active">
  <img src="img\solar\slide2.jpg" alt="" style="height:600px;width:100%;">
  </div>
</div>

<div class="container-fluid" style="position:relative;">
  <section class="p-0 pt-3 traco">
    <div class="container-fluid">
      <!-- SERVIÇOS background: #f2f2f2-->
      <div class="col-md-12 pt-5 pb-5 text-center services" style=" ">
        <h2>Prontos para Morar</h2>
        <img src="img/divider_line_big.svg" class="attachment-full size-full" alt="" data-attachment-id="47" data-permalink="https://themes.getmotopress.com/aquentro/home/divider_line_big/" data-orig-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-orig-size="" data-comments-opened="1" data-image-meta="[]" data-image-title="divider_line_big" data-image-description="" data-medium-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg" data-large-file="https://themes.getmotopress.com/aquentro/wp-content/uploads/sites/20/2018/07/divider_line_big.svg">
        <div class="paragraph d-flex flex-row justify-content-center">
          <p>
            
          </p>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="row" >
  <!-- d-flex flex-row -->
  <div class=" justify-content-center">
    <div class="col-lg-3 col-md-12">
      <a href="ed_vilaggio_palmeiras.php" rel="bookmark">
        <div class="card" >
          <img src="img\vilagio_palmeiras\fachada1.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h2 class="card-title">Villagio Palmeiras</h2>
            <p class="card-text">Villaggio Palmeiras - 2 qtos, 1 suíte, 2 vagas -57m2</p>
          </div>
          <!-- <button type="button" class="btn btn-primary"> <a href="ed_vilaggio_palmerias.php"> Detalhes </a></button> -->
        </div>
      </a>
    </div>

    <div class="col-lg-3 col-md-8 col-sm-12">
      <a href="ed_solar.php" rel="bookmark">
        <div class="card">
          <img src="img\solar\slide1.jpg" class="card-img-top" alt="...">
          <div class="card-body">
          <h2 class="card-title">Solar dos Montes</h2>
              <p class="card-text">Solar dos Montes - 3 qtos, 1 suíte, 2 vagas - 100m2</p>
          </div>
          <!-- <button type="button" class="btn btn-primary"> <a href="ed_solar.php"> Detalhes </a></button> -->
        </div>
      </a>
    </div>

    <div class="col-lg-3 col-md-8 col-sm-12">
      <a href="ed_sidonio.php" rel="bookmark">
        <div class="card">
          <img src="img/sidonio/slidefachada.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h2 class="card-title">Sidônio Maia</h2>
            <p class="card-text">Sidônio Maia - 2 qtos, 1 suíte, 2 vagas - 90m2</p>
          </div>
          <!-- <button type="button" class="btn btn-primary"> <a href="ed_sidonio.php"> Detalhes </a></button> -->
        </div>
      </a>
    </div>
  </div>
</div>

<div class="row mt-4 mb-4">
    <div class="d-flex flex-row justify-content-center">
      <div class="col-lg-3 col-md-8 col-sm-12">
        <a href="ed_parc_aquarius.php" rel="bookmark">
          <div class="card">
            <img src="img\parcaquarius\slidefachada.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h2 class="card-title">Parc Aquarius</h2>
              <p class="card-text">Parc Aquarius - 3 qtos, 1 suíte, 2 vagas - 77 e 84m2</p>
            </div>
            <!-- <button type="button" class="btn btn-primary"> <a href="ed_parc_aquarius.php"> Detalhes</a></button> -->
          </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-8 col-sm-12">
        <a href="ed_manhattan.php" rel="bookmark">
          <div class="card">
            <img src="img\manhattan\slide1.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h2 class="card-title">Manhattan</h2>
              <p class="card-text">Manhattan - 3 qtos, 1 suíte, 2 vagas - 92m2</p>
            </div>
            <!-- <button type="button" class="btn btn-primary"> <a href="ed_manhattan.php"> Detalhes </a></button> -->
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-8 col-sm-12">
        <a href="ed_jardim.php" rel="bookmark">
          <div class="card">
            <img src="img\jardim\slide1.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h2 class="card-title">Jardins</h2>
              <p class="card-text">Jardins - 3 qtos, 1 suíte, 2 vagas - 96m2</p>
            </div>
            <!-- <button type="button" class="btn btn-primary"> <a href="ed_jardim.php"> Detalhes</a></button> -->
          </div>
        </a>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="d-flex flex-row justify-content-center">

  
    <div class="col-lg-3 col-md-8 col-sm-12" >
      <a href="ed_mirante_sol.php" rel="bookmark">
        <div class="card">
          <img src="img\mirante\slide1.jpg" class="card-img-top" alt="...">
          <div class="card-body">
            <h2 class="card-title">Mirante do Sol</h2>
            <p class="card-text">Mirante do sol - 2 qtos, 1 suíte, 2 vagas - 57m2</p>
          </div>
          
        </div>
      </a>
    </div>
   
    </div>
  </div>
</div>


    <!-- Fim apartamentos avulsos -->
    <?php
    include("includes/footer.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js" type="module"></script>

    <script>
    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>

  <script>
    


    // MENU
    $("nav ul li a").click(function(){
      $("nav ul li a").removeClass("link-active");
      $(this).addClass("link-active");
    })

    $(document).scroll(function(e){
      let posicao = $(this).scrollTop();

        if(posicao > 122){
          $(".welcome").fadeIn(1000);
          $(".navbar-site").addClass("nav-fixed");
        }else{
          $(".navbar-site").removeClass("nav-fixed");
        }

        if(posicao > 800){
          $(".services").fadeIn(1000)
        }

        if(posicao > 1600){
          $(".rent").fadeIn(1000);
        }

        if(posicao > 2500){
          $(".locale").fadeIn(1000);
        }
      
        if(posicao > 2800){
          $(".numeros").show();
        }
      
    })

    $(document).ready(function(){
      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });
    })

  </script>
</body>
</html>